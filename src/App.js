import './App.css';
import {Component} from "react";

import CardList from "./components/CardList/CardList";
import SearchBox from "./components/SearchBox/SearchBox";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            monsters: [],
            searchField: ''
        };

        console.log('constructor')
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(users =>
                this.setState({monsters: users, filteredMonsters: users},
                    () => console.log(this.state))
            )
        console.log('component did mount')
    }

    onSearchChange = event => {
        const searchField = event.target.value.toLowerCase();
        this.setState({searchField})
    }

    render() {
        console.log('render')

        const {monsters, searchField} = this.state;
        const { onSearchChange } = this;

        const filteredMonsters = monsters.filter(monster => monster.name.toLowerCase().includes(searchField))

        return (
            <div className="App">
                <h1 className='app-title'>Monsters Rolodex </h1>
                <SearchBox className='monsters-search-box' placeholder='search monsters' onChangeHandler={onSearchChange}/>
                <CardList monsters={filteredMonsters} />
            </div>
        );
    }
}

export default App;
